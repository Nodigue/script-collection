#!/usr/bin/env python3

from pathlib import Path
from argparse import ArgumentParser

from PIL import Image

def main(args) -> None:
	"""
	Convert all webp images to png in a given directory.
	"""

	input_directory = args.input

	# Create a folder to store the converted images
	output_directory = input_directory / "png"
	output_directory.mkdir(parents = True, exist_ok = True)

	for file in input_directory.rglob("*.webp"):
		file_png_path = output_directory / f"{file.stem}.png"

		print(f"Converting file : [{file}] -> [{file_png_path}")

		# Load webp imaage
		image_webp = Image.open(file)

		# Save to png
		image_webp.save(file_png_path, format = "png", lossless = True)

if __name__ == "__main__":

	# Parse CLI arguments
	parser = ArgumentParser(description = "Convert all webp images to png in a given directory.")
	parser.add_argument("-i", "--input", type = Path, help = "Path of the input directory", default = Path("."))

	args = parser.parse_args()

	main(args)